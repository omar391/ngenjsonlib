package com.astronlab.json.reader;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Acer on 2/13/2016.
 */
public class JsonToCollection {
	private static ObjectMapper objectMapper = new ObjectMapper();

	public static <T> List<T> toList(String json, Class<T> listType)
			throws IOException {
		JavaType type = objectMapper.getTypeFactory().constructCollectionType(
				ArrayList.class, listType);
		List<T> list = convertToType(json, type);

		return list;
	}

	public static <K, V> Map<K, V> toMap(String json,
			Class<? extends Map<K, V>> mapType, Class<K> keyType, Class<V> valueType)
			throws IOException {
		JavaType type = objectMapper.getTypeFactory().constructMapType(mapType,
				keyType, valueType);
		Map<K, V> map = convertToType(json, type);

		return map;
	}

	private static <T> T convertToType(String jsonStr, JavaType type)
			throws IOException {
		return objectMapper.readValue(jsonStr, type);
	}
}