package com.astronlab.json.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Omar on 1/13/2016.
 */
public class RegexJsonParser {
	private String jsonStr;

	public RegexJsonParser(String json) {
		jsonStr = json;
	}

	public String getSingleValueByKey(String key) {
		Matcher matcher = getMatcher("\"" + key + "[\"\\\\:]+([^\"\\\\]+)");
		if (matcher.find()) {
			return matcher.group(1);
		}

		//match not found
		return null;
	}

	public List<List<String>> getGroupValuesByRegex(String regEx) {
		Matcher matcher = getMatcher(regEx);
		List arrayList = new ArrayList<>();

		while (matcher.find()) {
			List<String> values = new ArrayList<>();
			int count = matcher.groupCount();

			for (int i = 1; i <= count; i++) {
				values.add(matcher.group(i));
			}
			arrayList.add(values);
		}

		return arrayList;
	}

	public List<String> getAllSingleValuesByKey(String key) {
		List arrayList = new ArrayList<>();
		Matcher matcher = getMatcher("\"" + key + "[\"\\\\:]+([^\"\\\\]+)");
		while (matcher.find()) {
			arrayList.add(matcher.group(1));
		}

		return arrayList;
	}

	private Matcher getMatcher(String regex) {
		return Pattern.compile(regex).matcher(jsonStr);
	}
}
